<?php

namespace Bolt\Extension\Bamcocreate\Instabam;

use Vinkla\Instagram\Instagram;
use Bolt\Extension\SimpleExtension;

class InstabamExtension extends SimpleExtension
{
    const CACHE_FILE = 'bamcocreate/instabam.json';

    /**
     * {@inheritdoc}
     */
    protected function registerTwigFunctions()
    {
        return [
            'instabam' => 'instabamFunction'
        ];
    }

    /**
     * Return a list of recent posts by Instagram
     * @param $count Number of posts to retrieve
     * @return array
     * @throws \Vinkla\Instagram\InstagramException
     */
    public function instabamFunction($count)
    {
        $config = $this->getConfig();
        $app = $this->getContainer();
        $manager = $app['filesystem'];
        $cache = $manager->getFilesystem('cache')->getFile(self::CACHE_FILE);
        $results = [];

        if($cache->exists()) {
            $existing = json_decode($cache->read());
            if((int)$existing->timestamp + $config['cache_expiration'] > time()) {
                $results = $existing->data;
            }
        } else {
            $instagram = new Instagram($config['instagram_api_key']);
            $results = $instagram->get();
            $cache->put(json_encode(['timestamp' => time(), 'data' => $results]));
        }

        if(count($results) == 0) {
            $instagram = new Instagram($config['instagram_api_key']);
            $results = $instagram->get();
            $cache->put(json_encode(['timestamp' => time(), 'data' => $results]));
        }

        return array_slice($results, 0, $count);
    }

    /**
     * {@inheritdoc}
     */
    protected function isSafe()
    {
        return true;
    }

    /**
     * All the non-forms config keys.
     *
     * @return string[]
     */
    public function getConfigKeys()
    {
        return [
            'instagram_api_key',
            'instagram_account',
            'cache_expiration'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        return parent::getConfig();
    }

    /**
     * Set the defaults for configuration parameters.
     *
     * {@inheritdoc}
     */
    protected function getDefaultConfig()
    {
        return [
            'instagram_api_key' => '',
            'instagram_account' => 'bamcocreate',
            'cache_expiration' => 3600
        ];
    }
}
